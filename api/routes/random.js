var express = require('express');
var router = express.Router();
//get ALL tasks

/**
 * @api {get} api/rand/:date por Fecha
 * @apiName	byFecha
 * @apiGroup Disponibilidad
 *
 * @apiSuccess {String} name		Nombre del componente
 * @apiSuccess {String} date		Fecha de solicitud
 * @apiSuccess {String} disp 		Disponibilidad del componente (0,1)
 *
 * @apiParam   {String} date		Fecha de solicitud
 *
 * @apiSampleRequest http://localhost:3000/api/rand/:date
 *
 */

router.get('/rand/:date', function (req, res, next) {
	res.json({
			"name": componente,
			"date": req.params.date,
			"disp": Math.random().toFixed(2)
		});
});



/**
 * @api {get} /rand/:name/:date por Nombre y Fecha
 * @apiName	byNombreFecha
 * @apiGroup Disponibilidad
 *
 * @apiSuccess {String} name		Nombre del componente
 * @apiSuccess {String} date		Fecha de solicitud
 * @apiSuccess {String} disp 		Disponibilidad del componente (0,1)
 *
 * @apiParam   {String} date		Fecha de solicitud
 * @apiParam   {String} name		Nombre del componente
 *
 * @apiSampleRequest http://localhost:3000/api/rand/:name/:date
 *
 */

router.get('/rand/:name/:date', function (req, res, next) {
	if(req.params.name!=componente){
		res.json({'error':'Nombre de componente incorrecto'});
	}else{
		res.json({
			"name": componente,
			"date": req.params.date,
			"disp": Math.random().toFixed(2)
		});
	}
	
});

module.exports = router;