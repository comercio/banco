var express = require('express');

var mongojs = require('mongojs');
var ObjectID = require('mongodb').ObjectID;

var router = express.Router();

var db = mongojs('mongodb://banco:comercio@cluster0-shard-00-00-e1zzt.mongodb.net:27017,cluster0-shard-00-01-e1zzt.mongodb.net:27017,cluster0-shard-00-02-e1zzt.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin');

// CREATE OPERACION 
	// Aqui pasan Transferencias, Pago de Tarjetas de Credito y Pago de Facturas
router.post('/op', function (req, res, next) {

	
	console.log(req.body);

	operacion = req.body;

	var clientes = db.collection('clientes');
	var operaciones = db.collection('operaciones');


	// Traer de CLIENTES a origen
	var clienteOrigen = null;
	var clienteDestino = null;

	clientes.findOne({'usuario': operacion.idOrigen} , function (err, cOrigen){
 		if(err){
 			console.log('Error en consulta de clienteOrigen: ' + err );
 		}
 		//console.log('clienteOrigen: ' + JSON.stringify( clienteOrigen ));
 		clienteOrigen = cOrigen;
	
		if (operacion.tipo == 'transferencia') {

			// Traer de CLIENTES a destino
			clientes.findOne({'usuario': operacion.idDestino} , function (err, cDestino){
		 		if(err){
		 			console.log('Error en consulta de clienteDestino: ' + err );
		 		}else{

			 		//console.log('clienteDestino: ' + JSON.stringify( clienteDestino ));
			 		clienteDestino = cDestino;
			 		// Generar ID de operacion para agregarlo a las lista de operaciones de origen y destino 		
					var objectId = new ObjectID();

			 		if (operacion.tipoOrigen == 'ah') {
			 			if( parseFloat(clienteOrigen.saldoAh) - parseFloat(operacion.monto) >= 0){
			 				clienteOrigen.saldoAh = parseFloat(clienteOrigen.saldoAh) - parseFloat(operacion.monto); // Se debita
			 				clienteOrigen.opsAh.push(objectId); // Se agrega la operacion al cliente
			 				console.log('Queda en ah: ' + clienteOrigen.saldoAh);
			 				if(operacion.tipoDestino == 'ah'){
			 					clienteDestino.saldoAh = parseFloat(clienteDestino.saldoAh) + parseFloat(operacion.monto); //Se acredita
			 					clienteDestino.opsAh.push(objectId); // Se agrega la operacion al cliente
			 				} 					
			 				if(operacion.tipoDestino == 'ct'){
			 					clienteDestino.saldoCt = parseFloat(clienteDestino.saldoCt) + parseFloat(operacion.monto); //Se acredita
			 					clienteDestino.opsCt.push(objectId); // Se agrega la operacion al cliente
			 				}
			 			}
			 		}

			 		if (operacion.tipoOrigen == 'ct') {
			 			if( parseFloat(clienteOrigen.saldoCt) - parseFloat(operacion.monto) >= 0){
			 				clienteOrigen.saldoCt = parseFloat(clienteOrigen.saldoCt) - parseFloat(operacion.monto); // Se debita
			 				clienteOrigen.opsCt.push(objectId); // Se agrega la operacion al cliente
			 				console.log('Queda en ct: ' + clienteOrigen.saldoCt);
			 				if(operacion.tipoDestino == 'ah'){
			 					clienteDestino.saldoAh = parseFloat(clienteDestino.saldoAh) + parseFloat(operacion.monto); //Se acredita
			 					clienteDestino.opsAh.push(objectId); // Se agrega la operacion al cliente
			 				} 					
			 				if(operacion.tipoDestino == 'ct'){
			 					clienteDestino.saldoCt = parseFloat(clienteDestino.saldoCt) + parseFloat(operacion.monto); //Se acredita
			 					clienteDestino.opsCt.push(objectId); // Se agrega la operacion al cliente
			 				}
			 			}
			 		}				
				 	

			 		// Guardar clientes actualizados
							
			 		db.clientes.update({ _id : mongojs.ObjectId(clienteOrigen._id) }, clienteOrigen, {} , function (err, doc1){
			 			if(err){
			 				console.log('Error actualizando clienteOrigen: ' + clienteOrigen);		
			 			}

			 			db.clientes.update({ _id : mongojs.ObjectId(clienteDestino._id) }, clienteDestino, {} , function (err, doc2){
				 			if(err){
				 				console.log('Error actualizando clienteDestino: ' + clienteDestino);
				 			}

				 			// Guardar operacion en OPERACIONES agregando la fecha y el id
					 		operacion['_id'] = objectId;
					 		operacion['fecha'] = new Date();

					 		db.operaciones.save(operacion, function(err, doc3){
					 			if(err){
					 				res.send(err);			
					 			}
					 			console.log('Se realizó la tranferencia ' + doc3._id);
					 			res.redirect('/?user='+clienteOrigen.usuario); //Retornar a PosConsolidad despues de cualquier op ... se deberia enviar odigo de error
					 		});
				 		});

			 		});			 		
			 	}
		 	});

		}else if (operacion.tipo == 'pagoTarjeta' ) {
			
			var objectId = new ObjectID();
			// Validar saldo de tipo de cuenta origen contra monto
			// Validar deuda de tarjeta destino
			if(operacion.tipoOrigen == 'ah'){
				if(parseFloat(clienteOrigen.saldoAh) - parseFloat(operacion.monto) >= 0 ){
					if(parseFloat(clienteOrigen.limiteTar) - parseFloat(clienteOrigen.saldoTar) <= operacion.monto){
						clienteOrigen.saldoAh = parseFloat(clienteOrigen.saldoAh) - parseFloat(operacion.monto);
						clienteOrigen.saldoTar = parseFloat(clienteOrigen.saldoAh) + parseFloat(operacion.monto);
						clienteOrigen.opsAh.push(objectId);
					}
				}
			}
			
			if(operacion.tipoOrigen == 'ct'){
				if(parseFloat(clienteOrigen.saldoct) - parseFloat(operacion.monto) >= 0 ){
					if(parseFloat(clienteOrigen.limiteTar) - parseFloat(clienteOrigen.saldoTar) <= operacion.monto){
						clienteOrigen.saldoCt = parseFloat(clienteOrigen.saldoCt) - parseFloat(operacion.monto);
						clienteOrigen.saldoTar = parseFloat(clienteOrigen.saldoCt) + parseFloat(operacion.monto);
						clienteOrigen.opsCt.push(objectId);
					}
				}
			}

			// Generar id de operacion y agregarlo a las lista de operaciones de origen y destino
			
			clienteOrigen.opsTar.push(objectId);

			// Guardar operacion en OPERACIONES agregando la fecha y el id
	 		operacion['_id'] = objectId;
	 		operacion['fecha'] = new Date();
		 		db.operaciones.save(operacion, function(err, doc){
		 			if(err){
		 				res.send(err);			
		 			}
		 			console.log('Se realizó la tranferencia referente a ' + doc);
		 		});

		}else if (operacion.tipo == 'pagoFactura') {
			
			
			// Traer de FACTURAS a factura por pagar
			// Validar saldo de tipo de cuenta origen contra monto de la factura
			// Cambiar estado de la factura en FACTURAS
			// Cambiar de lista de Facturas





		}
		

	});
	

		
	});	


router.post('/botonPago', function(req,res,next) {

	

	operacion = req.body;

	var clientes = db.collection('clientes');
	var operaciones = db.collection('operaciones');

	var clienteOrigen = null;
	var clienteDestino = null;

	// Traer a clienteOrigen con el numero de tarjeta
	clientes.findOne({'numTar': ObjectID(operacion.numeroTarjeta)} , function (err, clienteOrigen){
 		if(err){
 			console.log('Error en consulta de clienteOrigen: ' + err );
 		}

 		if( clienteOrigen == null){
 			res.json({mensaje:'No existe tarjeta', cod : 1});
 		}else{

	 		// Verifico que la tarjeta tenga saldo suficiente para el monto

	 		if(parseFloat(clienteOrigen.saldoTar) > parseFloat(operacion.monto)){

	 			// Busco el usuarioDestino
	 			clientes.findOne({'usuario': operacion.usuarioDestino} , function (err, clienteDestino){
			 		if(err){
			 			console.log('Error en consulta de clienteDestino: ' + err );
			 		}

			 		if( clienteDestino == null){
	 					res.json({mensaje:'No existe usuarioDestino', cod : 2});
			 		}else{
			 			// se hace la transferencia
			 			var objectId = new ObjectID();	 		
			 			
		 				clienteOrigen.saldoTar = parseFloat(clienteOrigen.saldoTar) - parseFloat(operacion.monto); // Se le resta al saldo de la tarjeta
		 				clienteOrigen.opsTar.push(objectId); // Se agrega la operacion al cliente

		 				if(operacion.tipoDestino == 'ah'){
		 					clienteDestino.saldoAh = parseFloat(clienteDestino.saldoAh) + parseFloat(operacion.monto); //Se acredita
		 					clienteDestino.opsAh.push(objectId); // Se agrega la operacion al cliente
		 				}
		 					
		 				if(operacion.tipoDestino == 'ct'){
		 					clienteDestino.saldoCt = parseFloat(clienteDestino.saldoCt) + parseFloat(operacion.monto); //Se acredita
		 					clienteDestino.opsCt.push(objectId); // Se agrega la operacion al cliente
		 				}
				 		
				 		// Guardar clientes actualizados
				 		db.clientes.update({ _id : mongojs.ObjectId(clienteOrigen._id) }, clienteOrigen, {} , function (err, doc){
				 			if(err){
				 				res.send(err);			
				 			}
				 		});
				 		db.clientes.update({ _id : mongojs.ObjectId(clienteDestino._id) }, clienteDestino, {} , function (err, doc){
				 			if(err){
				 				res.send(err);			
				 			}
				 		});

				 		// Guardar operacion en OPERACIONES agregando la fecha y el id
				 		operacion['_id'] = objectId;
				 		operacion['fecha'] = new Date();
				 		db.operaciones.save(operacion, function(err, doc){
				 			if(err){
				 				res.send(err);			
				 			}
				 			doc['mensaje'] = 'exito';
				 			doc['cod'] = '-1';
				 			console.log('Se realizo pago contarjeta de credito a ' + doc.usuarioDestino);
				 			res.json(doc);
				 		});
					}
			 		
			 	});
	 		}else{
	 			es.json({err:'saldo insuficiente', cod : 0});
	 		}
	 	} 		
	});
});


router.get('/flush', function (req, res, next) {
	//Coneccion a BD
	var db = mongojs('mongodb://banco:comercio@cluster0-shard-00-00-e1zzt.mongodb.net:27017,cluster0-shard-00-01-e1zzt.mongodb.net:27017,cluster0-shard-00-02-e1zzt.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin');
	db.dropDatabase(function(err, dropped){
		  if (!err) console.log("It is dropped!");
		})
});



router.get('/create/:user', function (req, res, next) {
	//Coneccion a BD
	var db = mongojs('mongodb://banco:comercio@cluster0-shard-00-00-e1zzt.mongodb.net:27017,cluster0-shard-00-01-e1zzt.mongodb.net:27017,cluster0-shard-00-02-e1zzt.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin');
	var clientes = db.collection('clientes');
	var operaciones = db.collection('operaciones');

	console.log(req.params.user);

	db.clientes.save({
		usuario : req.params.user,
		contraseña : '123456',

		saldoAh : 10000.00,
		opsAh : [],

		saldoCt : 10000.00,
		opsCt : [],

		saldoTar : 200000.00,
		numTar :  new ObjectID(),
		limiteTar : 200000.00,
		opsTar : [],

		facPorPagar : [],
		facPorCobrar : []
	}, function(err, task){
	 			if(err){
	 				res.send(err);			
	 			}
	 			res.json(task);			
	 		});

});

router.get('/getClientes', function (req, res, next) {

	var clientes = db.collection('clientes');

	clientes.find({} , function (err, clienteOrigen){
 		if(err){
 			console.log('Error en consulta de clienteDestino: ' + err );
 		}
 		res.json( clienteOrigen );
 	});


});

router.get('/getOperaciones', function (req, res, next) {
	
	var operaciones = db.collection('operaciones');

	operaciones.find({} , function (err, clienteOrigen){
 		if(err){
 			console.log('Error en consulta de clienteDestino: ' + err );
 		}
 		res.json( clienteOrigen );
 	});


});


module.exports = router;