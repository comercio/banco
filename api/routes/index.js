var express = require('express');
var fecha = require('../models/fechas');
var mongojs = require('mongojs');

var router = express.Router();

router.get('/', function (req, res, next) {


	//Coneccion a BD
	var db = mongojs('mongodb://banco:comercio@cluster0-shard-00-00-e1zzt.mongodb.net:27017,cluster0-shard-00-01-e1zzt.mongodb.net:27017,cluster0-shard-00-02-e1zzt.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin');
	var clientes = db.collection('clientes');

	clientes.findOne({'usuario' : req.query.user } , function (err, cliente){
 		if(err){
 			console.log('Error en consulta de clienteDestino: ' + err );
 		}

 		if( cliente == null){
 			res.redirect('/login');

 		}else{
 			console.log('Inicia sesion: ' + cliente.usuario );
 			res.render('PosConsolidada.ejs', {elem: cliente});
 		}
 		
 	});
});

router.get('/login', function (req, res, next) {
	res.render('login.ejs',{});
});

router.post('/login', function (req, res, next) {
	console.log(req.body.usuario + ' ' + req.body.contraseña);

	//Coneccion a BD
	var db = mongojs('mongodb://banco:comercio@cluster0-shard-00-00-e1zzt.mongodb.net:27017,cluster0-shard-00-01-e1zzt.mongodb.net:27017,cluster0-shard-00-02-e1zzt.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin');
	var clientes = db.collection('clientes');

	clientes.findOne({'usuario' : req.body.usuario , 'contraseña' : req.body.contraseña } , function (err, cliente){
 		if(err){
 			console.log('Error en consulta de clienteDestino: ' + err );
 		}

 		if( cliente == null){
 			res.redirect('/login');

 		}else{
 			console.log('Inicia sesion: ' + cliente.usuario );
 			res.render('PosConsolidada.ejs', {elem: cliente});
 		}
 		
 	});
});

module.exports = router
