let express = require('express');
let path = require('path');
let bodyParser = require('body-parser');
let app = express();

//routes
let index = require('./api/routes/index');
let apiCore = require('./api/routes/core');








//Para renderizar html
app.set( 'views', path.join(__dirname, 'views') );
app.set( 'views engine', 'ejs' );


//Para los models de la aplicacion
app.use(express.static(path.join(__dirname,'models')));

app.use(express.static(path.join(__dirname,'views')));

app.use(express.static(path.join(__dirname,'views/images')));

//Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//cors de la aplicacion
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


//Definir rutas y sus controladores
app.use('/api', apiCore);
app.use('/', index);



//Levantamos el componente
if(process.argv[2]!=null){
	//Levantar el Servidor
	let port = parseInt(process.argv[2]);
	app.listen(port, function(){
		console.log('Servidor escuchando en el puerto ' + port );
	});

}else{
	console.log('Falta puerto');	
	
}